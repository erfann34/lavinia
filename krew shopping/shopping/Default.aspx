﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    

    <div class="center_content">
      <div class="oferta">

       <!-- Insert to your webpage where you want to display the slider -->
    <div class="amazingslider-wrapper" id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:585px;margin:0px auto 6px;">
        <div class="amazingslider" id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                <li><img src="imagesSlider/01.jpg" alt="01"  title="01" />
                </li>
                <li><img src="imagesSlider/02.jpg" alt="02"  title="02" />
                </li>
                <li><img src="imagesSlider/03.jpg" alt="03"  title="03" />
                </li>
                <li><img src="imagesSlider/04.jpg" alt="04"  title="04" />
                </li>
                <li><img src="imagesSlider/05.jpg" alt="05"  title="05" />
                </li>
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <li><img src="imagesSlider/01-tn.jpg" alt="01" title="01" /></li>
                <li><img src="imagesSlider/02-tn.jpg" alt="02" title="02" /></li>
                <li><img src="imagesSlider/03-tn.jpg" alt="03" title="03" /></li>
                <li><img src="imagesSlider/04-tn.jpg" alt="04" title="04" /></li>
                <li><img src="imagesSlider/05-tn.jpg" alt="05" title="05" /></li>
            </ul>
        <div class="amazingslider-engine"><a href="http://amazingslider.com" title="Responsive Slider jQuery">Responsive Slider jQuery</a></div>
        </div>
    </div>
    <!-- End of body section HTML codes -->
      </div>
      <div class="center_title_bar"style="text-align:right;padding-right:20px;padding-top:6px;">آخرین محصولات</div>
      <!-- *************** -->
      <div class="prod_box">
        <div class="center_prod_box">
          <div class="product_title"><a href="#">Samsung S20 Ultra</a></div>
          <div class="product_img"><a href="#"><img src="images/p2.jpg" alt="" border="0" /></a></div>
          <div class="prod_price"><span class="reduce">34,600,000 تومان</span><br /> <span class="price">32,600,000 تومان</span></div>
        </div>
        <div class="prod_details_tab"> <a href="#" class="prod_details">جزئیات</a> </div>
      </div>
      <div class="center_title_bar"style="text-align:right;padding-right:20px;padding-top:6px;">پیشنهاد ویژه</div>
            <!-- *************** -->
      <div class="prod_box">
        <div class="center_prod_box">
          <div class="product_title"><a href="#">Huawei P40 pro</a></div>
          <div class="product_img"><a href="#"><img src="images/p7.jpg" alt="" border="0" /></a></div>
          <div class="prod_price"><span class="reduce">23,790,000 تومان</span> <br /><span class="price">20,000,000 تومان</span></div>
        </div>
        <div class="prod_details_tab"> <a href="#" class="prod_details"> جزئیات</a> </div>
      </div>
    </div>



</asp:Content>

