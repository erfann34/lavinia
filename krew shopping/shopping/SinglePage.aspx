﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="true" CodeFile="SinglePage.aspx.cs" Inherits="SinglePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="center_content">
      <div class="center_title_bar"style="text-align:center;font-family:BTiBo;color:#106ead">جزئیات کالا</div>
      <div class="prod_box_big">
        <div class="center_prod_box_big">
          <div class="product_img_big"> <a href="javascript:popImage('images/big_pic.jpg','Some Title')" title="header=[Zoom] body=[&nbsp;] fade=[on]"><img src="images/p2.jpg" alt="" border="0" /></a>
            <div class="thumbs"> <a href="#" title="header=[Thumb1] body=[&nbsp;] fade=[on]"><img src="images/thumb1.jpg" alt="" border="0" /></a> <a href="#" title="header=[Thumb2] body=[&nbsp;] fade=[on]"><img src="images/thumb3.jpg" alt="" border="0" /></a> <a href="#" title="header=[Thumb3] body=[&nbsp;] fade=[on]"><img src="images/thumb2.jpg" alt="" border="0" /></a> </div>
          </div>
          <div class="details_big_box"style="text-align:right">
            <div class="product_title_big"style="font-family:ErasILig;color:#ff6a00">Samsung S20 Ultra</div>
            <div class="specifications"dir="rtl"style="font-family:BNaza"> وضعیت کالا: <span class="blue">موجود</span><br />
              گارانتی: <span class="blue">24 ماه</span><br /><span style="color:#000000"dir="rtl">
              فرستنده: </span><span class="blue">فرستنده کروکالا</span><br />
              شامل:<span class="blue"> TVA</span><br />
              توضیحات:<br /><span class="blue">نسل یازدهم از خانوداده Galaxy S سامسونگ این بار با نام «S20» در فوریه 2020 به علاقه‌مندان دنیای فناوری اطلاعات معرفی شد. سامسونگ برای سردمدار این خانواده عنوان «S20 Ultra» را انتخاب کرده است که نشان دهد این محصول فراتر از یک گوشی بالارده است. روکش محافظ Corning Gorilla Glass 6 این گوشی درکنار گواهینامه IP68 نشان از کیفیت ساخت بالای آن دارد. حسگر اثر انگشت این گوشی از نوع اولتراسونیک است و در زیر صفحه‌نمایش کار گذاشته شده است تا به‌‌سختی بتوان از عملکرد آن ایرادی گرفت. صفحه‌نمایشی با فناوری جدید Dynamic AMOLED 2X و تراکم پیکسلی 511 پیکسل هم با جزئیات و وضوح تصویر عالی ترجمه می‌شود. تراشه‌ی این محصول، اگزنوس 990 سامسونگ است که به‌عنوان تراشه‌ای 7 نانومتری عملکرد فوق‌العاده بهینه‌ای دارد. این تراشه که به همراه 16 گیگابایت رم عرضه می‌شود، یکی از قوی‌ترین تراشه‌های موجود در حال حاضر است و برای انجام بازی‌های سنگین و باز کردن چندین برنامه به‌صورت هم‌زمان و تماشای ویدئو کاملاً مناسب است و کم نمی‌آورد. تراشه‌ی گرافیکی Adreno 640 هم برای پخش ویدئو و بازی مناسب است. اما شاید مهم‌ترین ویژگی این محصول را می‌توان دوربین چهارگانه قاب پشتی آن دانست که از ترکیب حسگرهایی با رزولوشن 108، 48، 12 و حسگر TOF تشکیل شده است تا تصاویری خیره‌کننده را برای کاربر به همراه آورد. وجود درگاه USB Type-C، امکان شارژ بی‌سیم با سرعت بسیار بالا، پشتیبانی از فناوری شارژ سریع PD و باتری پرقدرت 5000 میلی‌آمپرساعتی از دیگر ویژگی‌های این غول جدید دنیای فناوری اطلاعات است. داشتن ویژگی‌هایی مانند پشتیبانی از شبکه ارتباطی 5G و امکان ضبط ویدئو با رزولوشن 8K نوید قدم گذاشتن به دنیایی نو را می‌دهند. دنیایی که در آن تهیه و توزیع محتوای تصویری به سرعت یک چشم برهم‌زدن انجام می‌شود. </span><br />
            </div>
            <div class="prod_price_big"style="font-family:BNaza; font-size:19px;"><span class="reduce">34,600,000 تومان</span><br /> <span class="price">32,600,000 تومان</span></div>
            <a href="#" class="prod_buy"style="font-family:BNaza;">افزودن به سبد خرید</a> <a href="#" class="prod_compare">مقایسه کالا</a> </div>
        </div>
      </div>
    </div>

</asp:Content>

